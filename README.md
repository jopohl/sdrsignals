# sdrsignals

Captures from IoT devices with Software Defined Radios (RTL-SDR, USRP N210 or HackRF)

| Filename   | Manufacturer | Description | Modulation | Bitlength | Sample rate (Sps) | Number Messages | Average Message Length (Byte) |
| -------- | --------    | --------    | --------   | -------- | -------- | -------- | -------- |
| action_FB_A_B_C_D | Action |  remote control (four buttons) for a LED light | OOK   | 500 | 2M | 19 | 11.95 |
| audi_open   | Audi |  Open command for an car  | OOK  | 2400   | 5M | 1 | 106 |
| bollard   | Unknown | Command to sink a bus bollard  | OOK  | 300   | 1M | 17 | 5 |
| brennenstuhl_ABCD_onoff  | Brennenstuhl | Remote control for wireless socket with four buttons  | OOK  | 300   | 1M | 64 | 13 | 
| elektromaten | Elektromaten |  command to open a parking gate  | OOK  | 600   | 2M | 11 | 17 
| esaver_4_on  | ESaver |  remote control (four buttons) for a wireless socket  | FSK  | 100  | 1M | 12 | 42  
| rwe_socket_pairing | RWE |  Pairing sequence for a wireless socket  | FSK  | 100 | 1M | 18 | 27.17
| scislo  | Scislo |  garage door open command  | FSK  | 200   | 500k | 8 | 64.75
| vw_open  | Volkswagen (VW) |  Open command for a car  | OOK  | 2500   | 1M | 1 | 53
| xavax  | Xavax |  command to set the temperature of a smart home system  | FSK  | 100 | 1M | 6 | 231.5

You can load all of these signals directly in the Universal Radio Hacker.